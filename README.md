# Information

Pre-configured stack to monitor servers managed with Docker and Traefik.

Included containers:
- [Prometheus](https://hub.docker.com/r/bitnami/prometheus) (non-root container)
- [cAdvisor](https://console.cloud.google.com/gcr/images/cadvisor/global/cadvisor)
- [NodeExporter](https://quay.io/repository/prometheus/node-exporter)
- [Grafana](https://hub.docker.com/r/bitnami/grafana) (non-root container)


Included dashboards:
- [NodeExporter](https://grafana.com/grafana/dashboards/1860) for host linux system
- [cAdvisor](https://grafana.com/grafana/dashboards/14282) for docker containers
- [Traefik general](https://grafana.com/grafana/dashboards/14918) for general traefik monitoring
- [Traefik service details](https://grafana.com/grafana/dashboards/4475) for detailled monitoring of a given service

# Usage

- Copy the `env.template` file to `.env` and configure the variables
- Execute the script `fix_permissions.sh` (fix the volume's permissions for non-root containers)
- Start the compose stack and try to access Grafana using the configured DNS

# Screenshots

## Server monitoring

![node_exporter](/uploads/fa08ff67fdecb186078fe8ab5cd192ba/node_exporter.png)

## Docker monitoring

![cadvisor](/uploads/8739a90fbc123cd90ecdae591d25a3fb/cadvisor.png)

## Traefik

![traefik_general](/uploads/acc92089daa83958c30f8bfd796b862a/traefik_general.png)

## Traefik service details

![traefik_details](/uploads/d0968d23f32d82ea5eb83a8e53ca72ba/traefik_details.png)
